# Demo of ad and label placement

This directory contains a small demo of the placement functionality and verbose
logging (to stderr). You need to have `admincer` installed and then run
`run.sh` and navigate to `out/index.html`.
